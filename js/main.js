/**
 * Ứng dụng quản lý sinh viên
 * Công việc:
 *   1.Tạo lớp đối tượng sinh viên
 *   2.Xây giao diện UI
 *   3.CRUD Sinh viên
 *   4.Lưu trữ sinh viên
 *   5.Tìm kiếm sinh viên (mã vs tên)
 *   6.Validation: kiểm tra dữ liệu
 */

const handleCreateStudent = function() {
    //1.dom input lấy value
    const id = document.getElementById("txtMaSV").value;
    const fullName = document.getElementById("txtTenSV").value;
    const type = document.getElementById("loaiSV").value;
    const math = +document.getElementById("txtDiemToan").value;
    const physics = +document.getElementById("txtDiemLy").value;
    const chemistry = +document.getElementById("txtDiemHoa").value;
    const trainingPoint = +document.getElementById("txtDiemRenLuyen").value;

    //tạo ra một object student lưu info
    const newStudent = new Student(id, fullName, type, math, physics, chemistry, trainingPoint);
    const promise = axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students",
        method: "POST",
        data: newStudent,
    });
    promise.then(function(res) {
        console.log(res.status);
        fetchStudents();
    }).catch(function(err) {
        console.log("Lỗi" + err.message);
    });
};
//Call API lưu student  vào DB

//yêu cầu: data phải là 1 array chưa đối tượng student
const createTable = function(data) {
    // if(!data){
    //   data = studentList
    // }

    // data = undefined || [{}]
    //nếu không truyền data thì data = studentList, còn nếu truyền data thì data = data

    var studentHTML = "";
    for (var i = 0; i < data.length; i++) {
        studentHTML += `<tr>
        <td> ${data[i].id} </td> 
        <td>${data[i].fullName}</td> 
        <td>${data[i].type}</td> 
        <td>${data[i].calcAverage()}</td> 
        <td>${data[i].trainingPoint}</td>
        <td>
            <button onclick="handleDeleteStudent('${
              data[i].id
            }')" style="width:40px; height:40px" class="btn btn-danger rounded-circle">
                <i class="fa fa-trash"></i>
            </button>
            <button onclick="handleGetUpdatedStudent('${
              data[i].id
            }')" style="width:40px; height:40px" class="btn btn-info rounded-circle">
                <i class="fa fa-pencil-alt"></i>
            </button>
        </td>
      </tr>`;
    }
    document.getElementById("tbodySinhVien").innerHTML = studentHTML;
};

//input: id sinh viên
const handleDeleteStudent = function(id) {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
        method: "DELETE",
    }).then(function(response) {
        console.log(response.status);
        fetchStudents();
    }).catch(function(error) {
        console.log(error);
    });
};

const handleUpdatedStudent = function(id) {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
        method: "PUT",
    }).then(function(response) {
        console.log(response.status);
        fetchStudents();
    }).catch(function(error) {
        console.log(error);
    });
};

const handleGetUpdatedStudent = function(id) {
    //đưa dữ liệu lên form
    // document.getElementById("txtMaSV").value = updatedStudent.id;
    // document.getElementById("txtTenSV").value = updatedStudent.fullName;
    // document.getElementById("loaiSV").value = updatedStudent.type;
    // document.getElementById("txtDiemToan").value = updatedStudent.math;
    // document.getElementById("txtDiemLy").value = updatedStudent.physics;
    // document.getElementById("txtDiemHoa").value = updatedStudent.chemistry;
    // document.getElementById("txtDiemRenLuyen").value =
    //   updatedStudent.trainingPoint;
    //call API gửi request yêu cầu lấy chi tiết sv
    console.log(id);
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
        method: "GET"
    }).then(function(response) {
        // console.log(response.data);
        document.getElementById("txtMaSV").value = response.data.id;
        document.getElementById("txtTenSV").value = response.data.fullName;
        document.getElementById("loaiSV").value = response.data.type;
        document.getElementById("txtDiemToan").value = response.data.math;
        document.getElementById("txtDiemLy").value = response.data.physics;
        document.getElementById("txtDiemHoa").value = response.data.chemistry;
        document.getElementById("txtDiemRenLuyen").value = response.data.trainingPoint;
        document.getElementById("txtMaSV").setAttribute("disabled", true);
    }).catch(function(error) {
        console.log(error);
    });
};

// Phần 2: luu dữ liệu sinh viên sửa vào hệ thống
const handleUpdateStudent = function() {
    //lấy dữ liệu người dùng mới sửa
    const id = document.getElementById("txtMaSV").value;
    const fullName = document.getElementById("txtTenSV").value;
    const type = document.getElementById("loaiSV").value;
    const math = +document.getElementById("txtDiemToan").value;
    const physics = +document.getElementById("txtDiemLy").value;
    const chemistry = +document.getElementById("txtDiemHoa").value;
    const trainingPoint = +document.getElementById("txtDiemRenLuyen").value;
    var updatedStudent = new Student(id, fullName, type, math, physics, chemistry, trainingPoint);
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
        method: "PUT",
        data: updatedStudent
    }).then(function(response) {
        console.log(response.status);
        document.getElementById("txtMaSV").setAttribute("disabled", false);
        fetchStudents();
    }).catch(function(error) {
        console.log(error);
    });


};
//Lấy danh sách sv từ API
const fetchStudents = function() {
    //promise:  -pending, -resolve (fulfill), reject
    const promise = axios({
            url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students",
            method: "GET",
        })
        //Res vs err tên tự đặt gì cũng đc
    promise.then(function(res) {
        console.log(res);
        var mappedData = mapData(res.data);
        console.log(mappedData);
        createTable(mappedData);
    }).catch(function(err) {
        console.log(err);

    });
};

//outpu: data cuar minh sau khi da map
const mapData = function(dataFromDB) {
    var mappedData = [];
    for (var i = 0; i < dataFromDB.length; i++) {
        var id = dataFromDB[i].id;
        var fullName = dataFromDB[i].fullName;
        var type = dataFromDB[i].type;
        var math = dataFromDB[i].math
        var physics = dataFromDB[i].physics;
        var chemistry = dataFromDB[i].chemistry;
        var trainingPoint = dataFromDB[i].trainingPoint;
        var mappedStudent = new Student(id, fullName, type, math, physics, chemistry, trainingPoint);
        mappedData.push(mappedStudent);
    }
    return mappedData;
}
fetchStudents();